FROM golang:1.16-alpine 

LABEL maintaner="I Me My Self"

COPY . .

EXPOSE 8080

CMD ["./main"]
